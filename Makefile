# pathparanoid makefile

INSTALL?=	install -p
PREFIX?=	/usr/local
SBIN?=		${PREFIX}/sbin


all:
	@echo "Nothing to do."

install:
	${INSTALL} -d ${DESTDIR}/var/lib/pathparanoid -m 700 -o root -g root
	${INSTALL} -m 0700 -o root -g root pathparanoid ${DESTDIR}${SBIN}
